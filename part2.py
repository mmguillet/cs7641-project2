"""
=== Part 2 - Requirements ===
part 2 - uses part 1 stuff as a component in the NN.  we do training, then check NN with test set of data. 

TODO: Use RHC, SA, and the chosen GA to find good weights for a Neural Network.  Using the same NN for one dataset from project 1, instead of backpropagation swap in these algorithms.

TODO: Look at both the accuracy charts and the loss charts to get the full story for report analysis.

Potential Pitfalls:
* The weights in a neural network are continuous and real-valued instead of discrete so you might want to think a little bit about what it means to apply these sorts of algorithms in such a domain.
* There are different loss and activation functions for NNs. If you use different libraries across your assignments, you need to make sure those are the same. For example, if you used scikit-learn and don’t modify the ABAGAIL example, they are not.

=== Other Notes ===
* TODO: either cross validation or single split, but very first thing needs to separate train/test and don't touch test data
* Use learning curves from project 1 as part of the process, don't need to include them in the report
* TODO: graphs
  * Need a loss curve that shows convergence
  * Need to plot training performance and cross validation performance as a function of number of iterations
  * Need/focus on convergence plots
* TODO: Ultimately need to tune some hyperparameters of NN
  * include how HPs tuned in analysis, provide examples of changes.  NN can use same number of nodes and hidden layers values from project 1, but we are replacing backprop with something totally different.
* TODO: REPORT: We can discuss that these methods do not use backpropagation, there is no going back with error, discuss the error we make does not backpropagate.
* 
"""
