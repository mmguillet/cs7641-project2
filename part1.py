"""
=== Part 1 - Requirements ===
part 1 - just tries to optimize a function and give you the best fitness score it can

TODO: implement four local random search algorithms https://mlrose.readthedocs.io/en/stable/source/algorithms.html
# RHC   - Randomized Hill Climbing
# SA    - Simulated Annealing
# GA    - a Genetic Algorithm
# MIMIC - https://www.cc.gatech.edu/~isbell/papers/isbell-mimic-nips-1997.pdf

TODO: 3 optimization problem domains. For the purpose of this assignment an "optimization problem" is just a fitness function one is trying to maximize (as opposed to a cost function one is trying to minimize). The problems you create should be over discrete-valued parameter spaces. Bit strings are preferable.

TODO: Apply all 4 search techniques (algorithms) to the 3 optimization problems.
# Problem 1 - highlight advantages of your genetic algorithm
# Problem 2 - highlight advantages of simulated annealing
# Problem 3 - highlight advantages of MIMIC

=== Other Notes ===
# TODO: Need to do graphs of 
  # fitness score performance vs. iterations 
    # do not need performance vs. training set size for this assignment
    # can use function evals instead of iterations? but don't?
    # Need/focus on convergence plots
    # Want to get highest fitness value in shortest amount of time
  # y-axis = fitness value, x-axis = hyperparameter values
# TODO: Sometimes can't converge, or find global max.  We can define convergence criteria. Either run algorithm until convergence (might take forever), or some number of iterations.  Usually criteria is based on when fitness function result does not change much.
  # Set a stop criteria based on low performance change/gain over the last X iterations.
  # Run 10, 20, 100 times and average the results.
# GA / MIMIC = good with structure, not jumping from 1 instance to another
# RHC / SA = good jumping between instances, not good with structure?
# Need to tune hyperparameters for every algorithm for each problem
  # this could be 15 times = 3 problems # 5 complexities/sizes
# If the problem is supposed to highlight a specific algorithm, increase complexity to see difference
# They just care about analysis describing hyperparameters in terms of our results, do not describe why the hyperparameter exists
# 

"""
import time
import numpy as np
import matplotlib.pyplot as plt
import mlrose_hiive as mlrose

# used for Max K Color problem generation
import networkx as nx
from mlrose_hiive.opt_probs import MaxKColorOpt

# Global Variables
random_state = 55   # same as project1
restarts = 100      # for RHC
curve = True        # used to get mlrose fitness values for each iteration

"""
Algorithms
"""

# RHC   - Randomized Hill Climbing
# https://mlrose.readthedocs.io/en/stable/source/algorithms.html#mlrose.algorithms.random_hill_climb
def rhc(problem, max_attempts, max_iters, init_state=None):
  print(f'Randomized Hill Climbing')

  # start time
  start = time.time()

  best_state, best_fitness, curve_fitness_values = mlrose.random_hill_climb(
    problem,
    max_attempts=max_attempts,
    max_iters=max_iters,
    restarts=restarts,
    init_state=init_state,
    curve=curve,
    random_state=random_state
  )

  # end time
  elapsed = round(time.time() - start, 4)  # round to 4 places

  print(f'best_state: {best_state}')
  print(f'best_fitness: {best_fitness}')
  print(f'curve_fitness_values: {curve_fitness_values}')
  print(f'curve_fitness_values.size: {curve_fitness_values.size}')
  print(f'Finished in [{elapsed}] seconds')

  return curve_fitness_values, elapsed


# SA    - Simulated Annealing
def sa(problem, max_attempts, max_iters, init_state=None, schedule = 'ExpDecay'):
  print(f'Simulated Annealing')

  print(f'Decay schedule: {schedule}')

  if schedule == 'ExpDecay':
    schedule = mlrose.ExpDecay()

  if schedule == 'GeomDecay':
    schedule = mlrose.GeomDecay()

  if schedule == 'ArithDecay':
    schedule = mlrose.ArithDecay()

  # start time
  start = time.time()

  best_state, best_fitness, curve_fitness_values = mlrose.simulated_annealing(
    problem,
    schedule = schedule,
    max_attempts = max_attempts,
    max_iters = max_iters,
    init_state = init_state,
    curve=curve,
    random_state = random_state
  )

  # end time
  elapsed = round(time.time() - start, 4)  # round to 4 places

  print(f'best_state: {best_state}')
  print(f'best_fitness: {best_fitness}')
  print(f'curve_fitness_values: {curve_fitness_values}')
  print(f'curve_fitness_values.size: {curve_fitness_values.size}')
  print(f'Finished in [{elapsed}] seconds')

  return curve_fitness_values, elapsed


# GA    - a Genetic Algorithm
def ga(problem, max_attempts, max_iters, pop_size=200, mutation_prob=0.1):
  print(f'Genetic Algorithm')

  print(f'pop_size: {pop_size}')
  print(f'mutation_prob: {mutation_prob}')

  # start time
  start = time.time()

  best_state, best_fitness, curve_fitness_values = mlrose.genetic_alg(
    problem,

    pop_size=pop_size,
    mutation_prob=mutation_prob,

    max_attempts = max_attempts,
    max_iters = max_iters,
    curve=curve,
    random_state = random_state
  )

  # end time
  elapsed = round(time.time() - start, 4)  # round to 4 places

  print(f'best_state: {best_state}')
  print(f'best_fitness: {best_fitness}')
  print(f'curve_fitness_values: {curve_fitness_values}')
  print(f'curve_fitness_values.size: {curve_fitness_values.size}')
  print(f'Finished in [{elapsed}] seconds')

  return curve_fitness_values, elapsed

# MIMIC - https://www.cc.gatech.edu/~isbell/papers/isbell-mimic-nips-1997.pdf
# https://mlrose.readthedocs.io/en/stable/source/algorithms.html#mlrose.algorithms.mimic
def mimic(problem, max_attempts, max_iters, pop_size=200, keep_pct=0.1):
  print(f'MIMIC')

  print(f'pop_size: {pop_size}')
  print(f'keep_pct: {keep_pct}')

  # start time
  start = time.time()

  best_state, best_fitness, curve_fitness_values = mlrose.mimic(
    problem,

    pop_size=pop_size,
    keep_pct=keep_pct,

    max_attempts = max_attempts,
    max_iters = max_iters,
    curve=curve,
    random_state = random_state
  )

  # end time
  elapsed = round(time.time() - start, 4)  # round to 4 places

  print(f'best_state: {best_state}')
  print(f'best_fitness: {best_fitness}')
  print(f'curve_fitness_values: {curve_fitness_values}')
  print(f'curve_fitness_values.size: {curve_fitness_values.size}')
  print(f'Finished in [{elapsed}] seconds')

  return curve_fitness_values, elapsed

"""
Problems
"""

# Problem 1 - nQueens

"""
Utility
"""
def plot_fitness_iterations(plot_name, curve_fitness_values, xstep=50, save=True):
  # plot fitness graph test
  x_values = np.arange(0, curve_fitness_values.size, 1)  # start x-axis at 1
  x_ticks = np.arange(0, curve_fitness_values.size + xstep, xstep)  
  # plot(x,y)
  plt.plot(x_values, curve_fitness_values)

  plt.xticks(x_ticks)  # start x-axis at 1
  plt.title('Fitness vs. Iterations')
  plt.ylabel('Fitness')
  plt.xlabel('Iterations')

  if save:
    plt.savefig('charts/part1/' + plot_name + '.png')


def queens_plot(size, plot_name, curve_fitness_values, xstep=50, save=True):
  # plot fitness graph test
  x_values = np.arange(0, curve_fitness_values.size, 1)  # start x-axis at 1
  x_ticks = np.arange(0, curve_fitness_values.size + xstep, xstep)  
  # plot(x,y)
  plt.plot(x_values, curve_fitness_values, label=f'{size} queens')

  plt.xticks(x_ticks)  # start x-axis at 1
  plt.title('n-Queens Fitness vs. Iterations')
  plt.ylabel('Fitness')
  plt.xlabel('Iterations')
  plt.legend(loc='lower right')

  if save:
    plt.savefig('charts/part1/' + plot_name + '.png')

def four_peaks_plot(t_pct, plot_name, curve_fitness_values, xstep=50, save=True):
  # plot fitness graph test
  x_values = np.arange(0, curve_fitness_values.size, 1)  # start x-axis at 1
  x_ticks = np.arange(0, curve_fitness_values.size + xstep, xstep)  
  # plot(x,y)
  plt.plot(x_values, curve_fitness_values, label=f'{t_pct} t_pct')

  plt.xticks(x_ticks)  # start x-axis at 1
  plt.title('Four Peaks Fitness vs. Iterations')
  plt.ylabel('Fitness')
  plt.xlabel('Iterations')
  plt.legend(loc='lower right')

  if save:
    plt.savefig('charts/part1/' + plot_name + '.png')

def mkc_plot(nodes, plot_name, curve_fitness_values, xstep=50, save=True):
  # plot fitness graph test
  x_values = np.arange(0, curve_fitness_values.size, 1)  # start x-axis at 1
  x_ticks = np.arange(0, curve_fitness_values.size + xstep, xstep)  
  # plot(x,y)
  plt.plot(x_values, curve_fitness_values, label=f'{nodes} nodes')

  plt.xticks(x_ticks)  # start x-axis at 1
  plt.title('Max K Color Fitness vs. Iterations')
  plt.ylabel('Fitness')
  plt.xlabel('Iterations')
  plt.legend(loc='lower right')

  if save:
    plt.savefig('charts/part1/' + plot_name + '.png')

"""
Redefine class here to have fitness function that maximizes
"""
class MaxKColor2:
  """Fitness function for Max-k color optimization problem. Evaluates the
  fitness of an n-dimensional state vector
  :math:`x = [x_{0}, x_{1}, \\ldots, x_{n-1}]`, where :math:`x_{i}`
  represents the color of node i, as the number of pairs of adjacent nodes
  of the same color.

  Parameters
  ----------
  edges: list of pairs
      List of all pairs of connected nodes. Order does not matter, so (a, b)
      and (b, a) are considered to be the same.

  Example
  -------
  .. highlight:: python
  .. code-block:: python

      >>> import mlrose_hiive
      >>> import numpy as np
      >>> edges = [(0, 1), (0, 2), (0, 4), (1, 3), (2, 0), (2, 3), (3, 4)]
      >>> fitness = mlrose_hiive.MaxKColor(edges)
      >>> state = np.array([0, 1, 0, 1, 1])
      >>> fitness.evaluate(state)
      3

  Note
  ----
  The MaxKColor fitness function is suitable for use in discrete-state
  optimization problems *only*.
  """

  def __init__(self, edges):

      # Remove any duplicates from list
      edges = list({tuple(sorted(edge)) for edge in edges})

      self.graph_edges = None
      self.edges = edges
      self.prob_type = 'discrete'

  def evaluate(self, state):
      """Evaluate the fitness of a state vector.

      Parameters
      ----------
      state: array
          State array for evaluation.

      Returns
      -------
      fitness: float
          Value of fitness function.
      """

      fitness = 0

      # this is the count of neigbor nodes with the same state value.
      # Therefore state value represents color.
      # This is NOT what the docs above say.

      if self.graph_edges is not None:
          fitness = sum(int(state[n1] == state[n2]) for (n1, n2) in self.graph_edges)
      else:
          fitness = 0
          for i in range(len(self.edges)):
              # Check for adjacent nodes of the same color
              if state[self.edges[i][0]] == state[self.edges[i][1]]:
                  fitness += 1

      return fitness * -1

  def get_prob_type(self):
      """ Return the problem type.

      Returns
      -------
      self.prob_type: string
          Specifies problem type as 'discrete', 'continuous', 'tsp'
          or 'either'.
      """
      return self.prob_type

  def set_graph(self, graph):
      self.graph_edges = [e for e in graph.edges()]


# generate mkc problem
# https://github.com/hiive/mlrose/blob/master/mlrose_hiive/generators/max_k_color_generator.py
def get_mkc_problem(seed, number_of_nodes=20, max_connections_per_node=4, max_colors=None):
  np.random.seed(seed)
  # all nodes have to be connected, somehow.
  node_connection_counts = 1 + np.random.randint(max_connections_per_node, size=number_of_nodes)

  node_connections = {}
  nodes = range(number_of_nodes)
  for n in nodes:
    all_other_valid_nodes = [o for o in nodes if (o != n and (o not in node_connections or
                                                              n not in node_connections[o]))]
    count = min(node_connection_counts[n], len(all_other_valid_nodes))
    other_nodes = sorted(np.random.choice(all_other_valid_nodes, count, replace=False))
    node_connections[n] = [(n, o) for o in other_nodes]

  # check connectivity
  g = nx.Graph()
  g.add_edges_from([x for y in node_connections.values() for x in y])

  for n in nodes:
    cannot_reach = [(n, o) if n < o else (o, n) for o in nodes if o not in nx.bfs_tree(g, n).nodes()]
    for s, f in cannot_reach:
      g.add_edge(s, f)
      check_reach = len([(n, o) if n < o else (o, n) for o in nodes if o not in nx.bfs_tree(g, n).nodes()])
      if check_reach == 0:
        break

  edges = [(s, f) for (s, f) in g.edges()]

  # custom fitness function for maximizing
  mkc_fitness = MaxKColor2(edges)

  problem = MaxKColorOpt(edges=edges, length=number_of_nodes, max_colors=max_colors, fitness_fn=mkc_fitness, maximize=True)
  # problem = MaxKColorOpt(edges=edges, length=number_of_nodes, max_colors=max_colors, maximize=True)
  return problem
  


if __name__ == '__main__':


  """
  Queens setup
  """
  max_attempts = 350 # will stop at attempt number (max_attempts + 1) if the same fitness score is returned this number of times in a row
  max_iters = 350    # may keep going until this number of tries have completed, if no better fitness score found before (max_attempts + 1) then it will stop early

  print(f'max_attempts: {max_attempts}')
  print(f'max_iters: {max_iters}')

  # built in fitness function for minimizing
  # fitness = mlrose.Queens()

  # custom fitness function for maximizing
  def queens_max(state):
    return mlrose.Queens().evaluate(state) * -1

  fitness_max = mlrose.CustomFitness(queens_max)

  init_state_list = [
    np.arange(0,8,1),   # 8 queens
    np.arange(0,16,1),  # 16 queens
    np.arange(0,32,1),  # 32 queens
  ]

  print(f'===')
  print(f'START Queens')
  print(f'===')

  """
  Queens - RHC
  """
  # run for 8/16/32 queens
  for init_state in init_state_list:
    print(f'init_state.size: {init_state.size}')
    problem = mlrose.DiscreteOpt(length=init_state.size, fitness_fn=fitness_max, maximize=True, max_val=init_state.size)
    print(f'init_state: {init_state}')
    curve_fitness_values, elapsed = rhc(problem, max_attempts, max_iters, init_state)

    plot_name = f'{round(time.time())}_rhc_{init_state.size}-queens_{max_attempts}-max-attempts_{max_iters}-max-iters_{elapsed}-elapsed'
    queens_plot(init_state.size, plot_name, curve_fitness_values)
  
  # clear plot after all 3 problem sizes
  plt.close()


  """
  Queens - SA
  """
  schedule_list = [
    'ExpDecay',
    'GeomDecay',
    'ArithDecay'
  ]

  # run for 8/16/32 queens
  for schedule in schedule_list:
    for init_state in init_state_list:
      print(f'init_state.size: {init_state.size}')
      problem = mlrose.DiscreteOpt(length=init_state.size, fitness_fn=fitness_max, maximize=True, max_val=init_state.size)
      print(f'init_state: {init_state}')
      # curve_fitness_values, elapsed = rhc(problem, max_attempts, max_iters, init_state)
      curve_fitness_values, elapsed = sa(problem, max_attempts, max_iters, init_state, schedule)

      plot_name = f'{round(time.time())}_sa-{schedule}_{init_state.size}-queens_{max_attempts}-max-attempts_{max_iters}-max-iters_{elapsed}-elapsed'
      
      # plot once for all problem sizes
      save_plot = True
      if init_state.size < 32:
        save_plot = False

      queens_plot(init_state.size, plot_name, curve_fitness_values, save=save_plot)

    # Clear plot after plotting the 3 sizes of problem
    plt.close()

  """
  Queens - GA
  """
  ga_hp_list = [
    [50, 0.1],  # [pop_size, mutation_prob]
    [100, 0.1],
    [150, 0.1],
    [50, 0.2],
    [100, 0.2],
    [150, 0.2],
  ]

  # run for 8/16/32 queens
  for hp_pair in ga_hp_list:
    pop_size = hp_pair[0]
    mutation_prob = hp_pair[1]

    for init_state in init_state_list:
      print(f'init_state.size: {init_state.size}')
      problem = mlrose.DiscreteOpt(length=init_state.size, fitness_fn=fitness_max, maximize=True, max_val=init_state.size)
      print(f'init_state: not used for GA')
      curve_fitness_values, elapsed = ga(problem, max_attempts, max_iters, pop_size=pop_size, mutation_prob=mutation_prob)

      plot_name = f'{round(time.time())}_ga_{init_state.size}-queens_{max_attempts}-max-attempts_{max_iters}-max-iters_{pop_size}-pop-size_{mutation_prob}-mutation-prob_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if init_state.size < 32:
        save_plot = False
    
      queens_plot(init_state.size, plot_name, curve_fitness_values, save=save_plot)
  
    # clear plot after all 3 problem sizes
    plt.close()


  """
  Queens - MIMIC
  https://mlrose.readthedocs.io/en/stable/source/algorithms.html#mlrose.algorithms.mimic
  """
  mimic_hp_list = [
    [50, 0.1],  # [pop_size, keep_pct]
    [100, 0.1],
    [150, 0.1],
    [50, 0.2],
    [100, 0.2],
    [150, 0.2],
  ]

  # run for 8/16/32 queens
  for hp_pair in mimic_hp_list:
    pop_size = hp_pair[0]
    keep_pct = hp_pair[1]

    for init_state in init_state_list:
      print(f'init_state.size: {init_state.size}')
      problem = mlrose.DiscreteOpt(length=init_state.size, fitness_fn=fitness_max, maximize=True, max_val=init_state.size)
      problem.set_mimic_fast_mode(True)
      
      print(f'init_state: not used for MIMIC')
      curve_fitness_values, elapsed = mimic(problem, max_attempts, max_iters, pop_size=pop_size, keep_pct=keep_pct)

      plot_name = f'{round(time.time())}_mimic_{init_state.size}-queens_{max_attempts}-max-attempts_{max_iters}-max-iters_{pop_size}-pop-size_{keep_pct}-keep-pct_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if init_state.size < 32:
        save_plot = False
    
      queens_plot(init_state.size, plot_name, curve_fitness_values, save=save_plot)
  
    # clear plot after all 3 problem sizes
    plt.close()

  print(f'===')
  print(f'END Queens')
  print(f'===')


  """
  Four Peaks
  """

  print(f'===')
  print(f'START Four Peaks')
  print(f'===')

  #  For large values of T, this problem becomes increasingly more difficult because the basin of attraction for the inferior local maxima become larger (https://www.cc.gatech.edu/~isbell/tutorials/mimic-tutorial.pdf page8, sect 6.1)
  fp_t_pct_list = [
    0.1,
    0.15,
    0.2
  ]

  fp_state = np.array([1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0])  # 30 values

  #  RHC
  for t_pct in fp_t_pct_list:
    fp_fitness = mlrose.FourPeaks(t_pct=t_pct)

    problem = mlrose.DiscreteOpt(length=fp_state.size, fitness_fn=fp_fitness, maximize=True, max_val=2)

    curve_fitness_values, elapsed = rhc(problem, max_attempts, max_iters, fp_state)

    plot_name = f'{round(time.time())}_rhc_4-peaks_{t_pct}-t-pct_{max_attempts}-max-attempts_{max_iters}-max-iters_{elapsed}-elapsed'

    # plot once for all problem sizes
    save_plot = True
    if t_pct < .2:
      save_plot = False

    four_peaks_plot(t_pct, plot_name, curve_fitness_values, save=save_plot)

  # clear plot after all 3 problem sizes
  plt.close()


  # SA
  for schedule in schedule_list:
    for t_pct in fp_t_pct_list:
      fp_fitness = mlrose.FourPeaks(t_pct=t_pct)

      problem = mlrose.DiscreteOpt(length=fp_state.size, fitness_fn=fp_fitness, maximize=True, max_val=2)

      curve_fitness_values, elapsed = sa(problem, max_attempts, max_iters, fp_state)

      plot_name = f'{round(time.time())}_sa_4-peaks_{t_pct}-t-pct_{max_attempts}-max-attempts_{max_iters}-max-iters_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if t_pct < .2:
        save_plot = False

      four_peaks_plot(t_pct, plot_name, curve_fitness_values, save=save_plot)

    # clear plot after all 3 problem sizes
    plt.close()
  
  # GA
  for hp_pair in ga_hp_list:
    pop_size = hp_pair[0]
    mutation_prob = hp_pair[1]

    for t_pct in fp_t_pct_list:
      fp_fitness = mlrose.FourPeaks(t_pct=t_pct)

      problem = mlrose.DiscreteOpt(length=fp_state.size, fitness_fn=fp_fitness, maximize=True, max_val=2)

      curve_fitness_values, elapsed = ga(problem, max_attempts, max_iters, pop_size=pop_size, mutation_prob=mutation_prob)

      plot_name = f'{round(time.time())}_ga_4-peaks_{t_pct}-t-pct_{max_attempts}-max-attempts_{max_iters}-max-iters_{pop_size}-pop-size_{mutation_prob}-mutation-prob_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if t_pct < .2:
        save_plot = False

      four_peaks_plot(t_pct, plot_name, curve_fitness_values, save=save_plot)

    # clear plot after all 3 problem sizes
    plt.close()
  
  # MIMIC
  for hp_pair in mimic_hp_list:
    pop_size = hp_pair[0]
    keep_pct = hp_pair[1]

    for t_pct in fp_t_pct_list:
      fp_fitness = mlrose.FourPeaks(t_pct=t_pct)

      problem = mlrose.DiscreteOpt(length=fp_state.size, fitness_fn=fp_fitness, maximize=True, max_val=2)
      problem.set_mimic_fast_mode(True)

      curve_fitness_values, elapsed = mimic(problem, max_attempts, max_iters, pop_size=pop_size, keep_pct=keep_pct)

      plot_name = f'{round(time.time())}_mimic_4-peaks_{t_pct}-t-pct_{max_attempts}-max-attempts_{max_iters}-max-iters_{pop_size}-pop-size_{keep_pct}-keep-pct_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if t_pct < .2:
        save_plot = False

      four_peaks_plot(t_pct, plot_name, curve_fitness_values, save=save_plot)

    # clear plot after all 3 problem sizes
    plt.close()

  print(f'===')
  print(f'END Four Peaks')
  print(f'===')


  print(f'===')
  print(f'START Max K Colors')
  print(f'===')

  
  mkc_nodes_list = [
    [20, np.array([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1])],  # [nodes, start state]
    [40, np.array([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1])],
    [60, np.array([0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1])],
  ]


  # RHC
  for mkc_pair in mkc_nodes_list:
    mkc_nodes = mkc_pair[0]
    mkc_state = mkc_pair[1]
    
    # https://github.com/hiive/mlrose/blob/master/mlrose_hiive/generators/max_k_color_generator.py
    problem = get_mkc_problem(random_state, mkc_nodes)
    
    curve_fitness_values, elapsed = rhc(problem, max_attempts, max_iters, mkc_state)

    plot_name = f'{round(time.time())}_rhc_{mkc_nodes}-mkc_nodes_{max_attempts}-max-attempts_{max_iters}-max-iters_{elapsed}-elapsed'

    # plot once for all problem sizes
    save_plot = True
    if mkc_nodes < 60:
      save_plot = False

    mkc_plot(mkc_nodes, plot_name, curve_fitness_values, save=save_plot)
  
  # clear plot after all 3 problem sizes
  plt.close()


  # SA
  for schedule in schedule_list:
    for mkc_pair in mkc_nodes_list:
      mkc_nodes = mkc_pair[0]
      mkc_state = mkc_pair[1]

      problem = get_mkc_problem(random_state, mkc_nodes)

      curve_fitness_values, elapsed = sa(problem, max_attempts, max_iters, mkc_state)

      plot_name = f'{round(time.time())}_sa-{schedule}_mkc_{mkc_nodes}-mkc_nodes_{max_attempts}-max-attempts_{max_iters}-max-iters_{elapsed}-elapsed'


      # plot once for all problem sizes
      save_plot = True
      if mkc_nodes < 60:
        save_plot = False

      mkc_plot(mkc_nodes, plot_name, curve_fitness_values, save=save_plot)
  
    # clear plot after all 3 problem sizes
    plt.close()
  
  # GA
  for hp_pair in ga_hp_list:
    pop_size = hp_pair[0]
    mutation_prob = hp_pair[1]

    for mkc_pair in mkc_nodes_list:
      mkc_nodes = mkc_pair[0]
      mkc_state = mkc_pair[1]

      problem = get_mkc_problem(random_state, mkc_nodes)

      curve_fitness_values, elapsed = ga(problem, max_attempts, max_iters, pop_size=pop_size, mutation_prob=mutation_prob)

      plot_name = f'{round(time.time())}_ga_mkc_{mkc_nodes}-mkc_nodes_{max_attempts}-max-attempts_{max_iters}-max-iters_{pop_size}-pop-size_{mutation_prob}-mutation-prob_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if mkc_nodes < 60:
        save_plot = False

      mkc_plot(mkc_nodes, plot_name, curve_fitness_values, save=save_plot)

    # clear plot after all 3 problem sizes
    plt.close()


  # MIMIC
  for hp_pair in mimic_hp_list:
    pop_size = hp_pair[0]
    keep_pct = hp_pair[1]

    for mkc_pair in mkc_nodes_list:
      mkc_nodes = mkc_pair[0]
      mkc_state = mkc_pair[1]

      problem = get_mkc_problem(random_state, mkc_nodes)
      problem.set_mimic_fast_mode(True)

      curve_fitness_values, elapsed = mimic(problem, max_attempts, max_iters, pop_size=pop_size, keep_pct=keep_pct)

      plot_name = f'{round(time.time())}_mimic_mkc_{mkc_nodes}-mkc_nodes_{max_attempts}-max-attempts_{max_iters}-max-iters_{pop_size}-pop-size_{keep_pct}-keep-pct_{elapsed}-elapsed'

      # plot once for all problem sizes
      save_plot = True
      if mkc_nodes < 60:
        save_plot = False

      mkc_plot(mkc_nodes, plot_name, curve_fitness_values, save=save_plot)

    # clear plot after all 3 problem sizes
    plt.close()


  print(f'===')
  print(f'END Max K Colors')
  print(f'===')
