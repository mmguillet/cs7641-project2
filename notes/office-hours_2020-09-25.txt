* can use mlrose hiive
* Part of the assignment is running against optimization problems, like Four Peaks as you mentioned.
  The other part is replacing backprop for your A1 data set with a randomized optimization algorithm.
* do at least 3 questions from the problem set, can submit scanned paper
* can use almost any optimization problem, but they should be represented as bit string
* need to do tuning for randomized optimization neural network (we should have backpropagation setup for neural network from project1)
* we will not be using backprop algorithm to calculate weights this time
* If using traveling salesman problem, they want us to play with the number of coordinates (complexity of the problem) and write analysis comparing how it affects the 4 algorithms (find the point where the problem becomes hard enough that 1 or 2 algorithms start to stand out as better than the others)
* 
