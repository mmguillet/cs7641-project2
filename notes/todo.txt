** Algorithms **
For the optimization problems - run RHC, SA, GA, ~and~ MIMIC
For the NN part - run randomized hill climbing (RHC), simulated annealing (SA), genetic algorithm (GA) -- not MIMIC


@34:40 mins
17. ** Still need to describe that we used learning curves and model complexity curves, just don't need to analyze them **
** Need to show graphs of how we chose hyperparameters and what behavior changed for each optimation problem, for one fixed complexity of each problem **
** Then look at performance given different complexities of each problem (with fixed hyperparameters), show graphs - iterations or # function evaluations until it converges **
** Performance vs. iterations, performance vs. problem size/complexity **
** Important to show how long it takes time wise to converge **


@38:00 mins
20. We can graph against either # iterations or # function evaluations
** Need to do graphs of fitness score performance vs. iterations **


24. ** Need to tune hyperparameters for every algorithm for each problem ** - this could be 15 times...

27. ** They just care about analysis describing hyperparameters in terms of our results, do not describe why the hyperparameter exists **
@48:50
** If the problem is supposed to highlight a specific algorithm, increase complexity to see difference. **
** GA / MIMIC = good with structure, not jumping from 1 instance to another **
** RHC / SA = good jumping between instances, not good with structure? **

@50:50 mins
29. 2 types of learning curves
** graph performance vs. iterations ** (we do not care about performance vs. training set size for this assignment)

** graph y-axis = fitness value, x-axis = hyperparameter values **

