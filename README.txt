# CS7641 Project 2
Matthew Guillet

**Note: Due to personal reasons did not have time to finish part 2

Code: https://bitbucket.org/mmguillet/cs7641-project2/src/master/

## Install and Run
```
# clone repo
git clone git@bitbucket.org:mmguillet/cs7641-project2.git

# go into project directory
cd cs7641-project2

# install python virtual environment
sudo apt install python3-venv

# setup new python virtual environment
python3 -m venv venv

# activate virtual environment
source venv/bin/activate

# update python module installer modules
pip install -U setuptools pip

# install python modules
pip install -r requirements.txt

# run code
# saves all print statements to file in /output directory with timestamp and saves graphs in /charts
python3 part1.py > ./output/`date +"%Y-%m-%dT%H%M%S_part1.txt"`
```

## Cite Sources
* https://mlrose.readthedocs.io/
* https://www.cc.gatech.edu/~isbell/papers/isbell-mimic-nips-1997.pdf
* https://stackoverflow.com
