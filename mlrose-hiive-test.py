import mlrose_hiive as mlrose
import numpy as np

# global variables
random_state = 55

# https://mlrose.readthedocs.io/en/stable/source/tutorial1.html

# define a fitness function
fitness = mlrose.Queens()

# Define alternative N-Queens fitness function for maximization problem
def queens_max(state):

  # Initialize counter
  fitness_cnt = 0

  # For all pairs of queens
  for i in range(len(state) - 1):
    for j in range(i + 1, len(state)):

        # Check for horizontal, diagonal-up and diagonal-down attacks
        if (state[j] != state[i]) \
            and (state[j] != state[i] + (j - i)) \
            and (state[j] != state[i] - (j - i)):

          # If no attacks, then increment counter
          fitness_cnt += 1

  return fitness_cnt

# Initialize custom fitness function object
# fitness_cust = mlrose.CustomFitness(queens_max)

problem = mlrose.DiscreteOpt(length = 8, fitness_fn = fitness, maximize = False, max_val = 8)

# Define decay schedule
schedule = mlrose.ExpDecay()

# Define initial state
init_state = np.array([0, 1, 2, 3, 4, 5, 6, 7])

# Solve problem using simulated annealing
best_state, best_fitness, curve_fitness_values = mlrose.simulated_annealing(
  problem,
  schedule = schedule,
  max_attempts = 100,
  max_iters = 1000,
  init_state = init_state,
  # random_state = 1
  random_state = random_state
)

print(best_state)
# [6 4 7 3 6 2 5 1]

print(best_fitness)
# 2.0



# # Solve problem using simulated annealing
# best_state, best_fitness = mlrose.simulated_annealing(problem, schedule = schedule,
#                                                       max_attempts = 100, max_iters = 1000,
#                                                       init_state = init_state, random_state = 1)

# print(best_state)
# # [4 1 3 5 7 2 0 6]

# print(best_fitness)
# # 0.0
